
import json
import os
from model import Model


class Log():

    """
    Used to load resources, to save and load models
    """

    def __init__(self):
        self.data = {}
        # If resources files don't exist, they are created
        if not os.path.isfile("resources/parameters.json") or not os.path.isfile("resources/hyper-parameters.json"):
            self.create_log_files()
        else:
            self.load_log()

    # Loads resources
    def load_log(self):
        with open("resources/parameters.json", "r") as file:
            parameters_json_data = json.load(file)
        with open("resources/hyper-parameters.json", "r") as file:
            hyper_parameters_json_data = json.load(file)
        json_data={}
        for key, value in parameters_json_data.items():
            json_data[key] = {**parameters_json_data[key], **hyper_parameters_json_data[key]}
            self.data[key] = Model.json_to_object(json_data[key])

    #  Create resources files
    def create_log_files(self):
        with open("resources/parameters.json", "w+") as file:
            json.dump({}, file, indent=2)
        with open("resources/hyper-parameters.json", "w+") as file:
            json.dump({}, file, indent=2)

    # Updates the resources files
    def update(self, data):
        # (!) to be transformed to a static method
        self.data = data
        parameters = {}
        hyper_parameters ={}
        for key, value in data.items():
            parameters[key], hyper_parameters[key]  = value.object_to_json()
        with open("resources/parameters.json", mode='w') as file:
            json.dump(parameters, file, indent=2)
        with open("resources/hyper-parameters.json", mode='w') as file:
            json.dump(hyper_parameters, file, indent=2)
