
from log import Log
from model import Model
from utils import *
from visualization import Visualization

"""
Importance of hyperparameters
1 - learning rate alpha
2 - beta1, beta2, epsilon (adam, rmsprop etc.) - beta1 si beta2 can't take value 1 (!), because the denominator would be = 0
2 - number of units in each layer
2 - mini-batch size
3 - number of layers in the network
3 - learning rate decay
"""

class Tunner():

    """
    Used to handle the training of different models. Its functions allow generating random hyper-parameters for each
    model, displaying cost graphics, loading previously trained models and saving the current ones in JSON files
    """

    def __init__(self, resources):
        self.log = Log()
        self.models = {}
        self.resources = resources

        # Loading the dataset
        self.X_train_orig, self.Y_train_orig, self.X_test_orig, self.Y_test_orig, classes = load_dataset(self.resources["train_dataset_path"], self.resources["test_dataset_path"])

        # Flatten the training and test images
        self.X_train = self.X_train_orig.reshape(self.X_train_orig.shape[0], -1).T
        self.X_test = self.X_test_orig.reshape(self.X_test_orig.shape[0], -1).T
        self.no_of_parameters = self.X_train.shape[0]
        self.lastIndex = 0
        self.last_layer_size = 6

    def create_threads(self, no_of_threads=1):

        """
        Creates one or more thread which will train models with different configuration

        Arguments:
        no_of_threads -- number of threads to be created
        """

        # Generate random architectures
        layers_dims = self.generate_layers_dims(no_of_threads)

        # Generate random optimizers
        optimizers = self.generate_optimizers(no_of_threads)

        # Generate random initializers
        initializations = self.generate_initializations(no_of_threads)

        # Generate random on/off values for batch normalization
        batch_norms = self.generate_batch_norms(no_of_threads)

        # Generate random learning rates
        learning_rates = self.generate_learning_rates(no_of_threads)

        # Generate random mini batch sizes
        mini_batch_sizes = self.generate_mini_batch_sizes(no_of_threads)

        # Generate random beta1 optimization hyper-parameters
        beta1s = self.generate_optimization_beta1s(no_of_threads)

        # Generate random beta2 optimization hyper-parameters
        beta2s = self.generate_optimization_beta2s(no_of_threads)
        epsilon = 1e-8

        # Generate random hidden layers activations
        activations = self.generate_activation(no_of_threads)

        # Generate random on/off values for dropouts
        dropouts = self.generate_dropouts(no_of_threads)
        batch_norm_eps = 1e-8
        softmax = self.resources["softmax"]

        # Loops over the number of threads
        for i in range(no_of_threads):
            # Creates a model for each thread
            model = Model('model_'+str(self.lastIndex), self.X_train, self.Y_train_orig, layers_dims[i], optimizers[i],
                          initializations[i], batch_norms[i], learning_rates[i][0], mini_batch_sizes[i][0],
                          beta1s[i][0], beta2s[i][0], epsilon, activations[i], dropouts[i], batch_norm_eps, softmax)
            self.models["model_"+str(self.lastIndex)] = model
            self.lastIndex += 1

    def load_threads(self):

        """
        Loads data of previously created threads in order to continue training
        """

        data = self.log.data
        for key, value in data.items():
            model = Model(data[key]["name"], self.X_train, self.Y_train_orig, data[key]["layers_dims"], data[key]["optimizer"],
                          data[key]["initialization"], data[key]["batch_norm"], data[key]["learning_rate"],
                          data[key]["mini_batch_size"], data[key]["beta1"], data[key]["beta2"], data[key]["epsilon"],
                          data[key]["activation"], data[key]["dropout"], data[key]["batch_norm_eps"],
                          data[key]["softmax"])
            model.resources["costs"] = data[key]["costs"]
            model.resources["parameters"] = data[key]["parameters"]
            model.resources["v"] = data[key]["v"]
            model.resources["s"] = data[key]["s"]
            model.resources["adam_counter"] = data[key]["adam_counter"]
            self.models[key] = model


    def view(self):

        """
        Displays the cost graphic associated to threads costs

        """

        visualization = Visualization()
        visualization.draw(self.models)


    def train(self, no_epochs):

        """
        Trains the models in the current threads

        Arguments:
        no_epochs -- number of epochs over which the training is run

        """

        for key, value in self.models.items():
            value.train(no_epochs)
            print(value.resources["name"] + ": train accuracy is " + str(value.predict(self.X_train, self.Y_train_orig)))
            print(value.resources["name"] + ": test accuracy is " + str(value.predict(self.X_test, self.Y_test_orig)))

        self.update()


    def update(self):

        """
        Saves current parameters and hyper-parameters for future use
        """

        self.log.update(self.models)


    def generate_layers_dims(self, no_of_threads=1, min_units=10, max_units=30, max_layers=3):

        """
        Generate random architectures

        Arguments:
        no_of_threads -- number of threads
        min_units --minimum number of units inside any of the layers
        max_units -- maximum number of units inside any of the layers
        max_layers -- maximum number of layers inside the network

        Returns:
        layers_dims -- list with each layer's dimensions
        """

        no_of_layers = np.random.randint(1, max_layers, size=no_of_threads)
        layers_dims = []
        for i in range(len(no_of_layers)):
            values = np.random.randint(min_units, max_units, no_of_layers[i])
            values = np.insert(values, 0, self.no_of_parameters,)
            values = np.append(values, self.last_layer_size)
            layers_dims.append(values)

        return layers_dims


    def generate_optimizers(self, no_of_threads=1):

        """
        Generate random optimizers

        Arguments:
        no_of_threads -- number of threads

        Returns:
        optimizers -- list of optimizer names for each threads
        """

        indexes = np.random.randint(len(Model.optimizers), size=no_of_threads)
        optimizers = []
        for index in range(len(indexes)):
            optimizers.append(Model.optimizers[indexes[index]])

        return optimizers


    def generate_initializations(self, no_of_threads=1):

        """
        Generate random initializers

        Arguments:
        no_of_threads -- number of threads

        Returns:
        optimizers -- list of parameters initializations names for each threads
        """

        indexes = np.random.randint(len(Model.initializations), size=no_of_threads)
        initializations=[]
        for index in range(len(indexes)):
            initializations.append(Model.initializations[indexes[index]])

        return initializations


    def generate_batch_norms(self, no_of_threads=1):

        """
        Generate random on/off (True/False) values for batch normalization

        Arguments:
        no_of_threads -- number of threads

        Returns:
        batch_norms -- list of True/False values, if batch norm is on/off
        """

        values=["True", "False"]
        indexes = np.random.randint(2, size=no_of_threads)
        batch_norms = []
        for index in range(len(indexes)):
            batch_norms.append(values[indexes[index]])

        return batch_norms


    def generate_learning_rates(self, no_of_threads=1, min_power=-4, max_power=0):

        """
        Generate random learning rates
        
        Arguments:
        no_of_threads -- number of threads
        min_power -- minimum power
        max_power -- maximum power

        Returns:
        learning_rates -- list of learning rate value for each thread
        """

        r = np.random.rand(no_of_threads,1) * (max_power - min_power) + min_power
        learning_rates = np.power(np.full(r.shape, 10), r)
        
        return learning_rates


    def generate_mini_batch_sizes(self, no_of_threads=1, min_power=5, max_power=12):

        """
        Generate random mini batch sizes
        
        Arguments:
        no_of_threads -- number of threads
        min_power -- minimum power
        max_power -- maximum power
        
        Returns:
        mini_batch_sizes -- list of mini batch size value for each thread
        """

        mini_batch_sizes = np.power(2, min_power+np.random.randint(max_power-min_power, size=(no_of_threads,1)))
        
        return mini_batch_sizes


    def generate_optimization_beta1s(self, no_of_threads=1, min_power=-1, max_power=-3):

        """
        Generate random beta1 optimization hyper-parameters

        Arguments:
        no_of_threads -- number of threads
        min_power -- minimum power
        max_power -- maximum power

        Returns:
        beta1s -- list of beta1 value for each thread
        """

        # order -1 -> calculating 1-value = 0.9
        # order -3 -> calculating 1-value = 0.999
        r = np.random.rand(no_of_threads, 1) * (max_power - min_power) + min_power
        values = 1-np.power(np.full(r.shape, 10), r)
        beta1s = np.sort(values, 0)

        return beta1s


    def generate_optimization_beta2s(self, no_of_threads=1, min_power=-1, max_power=-3):

        """
        Generate random beta2 optimization hyper-parameters

        Arguments:
        no_of_threads -- number of threads
        min_power -- minimum power
        max_power -- maximum power

        Returns:
        beta2s -- list of beta2 value for each thread
        """

        # order -1 -> calculating 1-value = 0.9
        # order -3 -> calculating 1-value = 0.999
        r = np.random.rand(no_of_threads, 1) * (max_power - min_power) + min_power
        values = 1-np.power(np.full(r.shape, 10), r)
        beta2s = np.sort(values, 0)

        return beta2s


    def generate_activation(self, no_of_threads=1):

        """
        Generate random hidden layers activations

        Arguments:
        no_of_threads -- number of threads

        Returns:
        activations -- list of hidden layers activation for each thread
        """

        indexes = np.random.randint(len(Model.activations), size=no_of_threads)
        activations=[]
        for index in range(len(indexes)):
            activations.append(Model.activations[indexes[index]])

        return activations


    def generate_dropouts(self, no_of_threads=1):

        """
        Generate random on/off (True/False) values for dropouts

        Arguments:
        no_of_threads -- number of threads

        Returns:
        dropouts -- list of True/False values, if dropout is on/off
        """

        values=["True", "False"]
        indexes = np.random.randint(2, size=no_of_threads)
        dropouts = []
        for index in range(len(indexes)):
            dropouts.append(values[indexes[index]])

        return dropouts
