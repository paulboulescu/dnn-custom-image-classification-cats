
import numpy as np
import h5py
import math
from scipy.special import expit


def random_mini_batches(x, y, mini_batch_size=2048, type="fc"):

    """

    Create a list of random minibatches from (X, Y)

    Arguments:
    x -- input data to be split into mini-batches
    y -- label data to be split into mini-batches
    mini_batch_size -- size of the mini-batches
    type -- 'fc' (fully connected) for [n, m] / 'cn' (convolutional) for [m, nh, nw, nc]

    Returns:
    mini_batches -- list of tuples (mini_batch_X, mini_batch_Y)

    """

    seed = 0
    np.random.seed(seed)

    # Number of training examples
    if type == "fc":
        m = x.shape[1]
    elif type == "cn":
        m = x.shape[0]

    mini_batches = []

    # Shuffle (X, Y)
    permutation = list(np.random.permutation(m))

    if type == "fc":
        shuffled_x = x[:, permutation]
        shuffled_y = y[:, (permutation)].reshape((y.shape[0], m))
    elif type == "cn":
        shuffled_x = x[permutation, :, :, :]
        shuffled_y = y[permutation, :]

    # Number of mini batches of size mini_batch_size
    num_complete_minibatches = math.floor(m / mini_batch_size)

    for k in range(0, int(num_complete_minibatches)):
        if type == "fc":
            mini_batch_x = shuffled_x[:, k * mini_batch_size:(k + 1) * mini_batch_size]
            mini_batch_y = shuffled_y[:, k * mini_batch_size:(k + 1) * mini_batch_size]
        elif type == "cn":
            mini_batch_x = shuffled_x[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :, :, :]
            mini_batch_y = shuffled_y[k * mini_batch_size: k * mini_batch_size + mini_batch_size, :]

        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batches.append(mini_batch)

    # Handling the end case, when last mini-batch < mini_batch_size
    if m % mini_batch_size != 0:
        if type == "fc":
            mini_batch_x = shuffled_x[:, num_complete_minibatches * mini_batch_size:m]
            mini_batch_y = shuffled_y[:, num_complete_minibatches * mini_batch_size:m]
        elif type == "cn":
            mini_batch_x = shuffled_x[num_complete_minibatches * mini_batch_size: m, :, :, :]
            mini_batch_y = shuffled_y[num_complete_minibatches * mini_batch_size: m, :]

        mini_batch = (mini_batch_x, mini_batch_y)
        mini_batches.append(mini_batch)

    return mini_batches


def sigmoid(z):

    """
    Implements the sigmoid activation

    Arguments:
    z -- the linear result of the forward propagation

    Returns:
    a -- the activation result of the forward propagation
    cache -- z, the linear forward stored for computing the backward propagation efficiently

    """

    # Previous formula, was causing an overflow error:
    # a = 1 / (1 + np.exp(-z))
    # Current formula
    a = expit(z)

    cache = z

    return a, cache


def sigmoid_backward(d_a, cache):

    """

    Implement the backward propagation for a single sigmoid unit

    Arguments:
    d_a -- gradient of the activation
    cache -- z, the linear forward stored for computing the backward propagation efficiently

    Returns:
    d_z -- gradient of the cost with respect to z

    """

    z = cache

    # Previous formula, was causing an overflow error:
    # s = 1 / (1 + np.exp(-z))
    # Current formula
    s = expit(z)

    d_z = d_a * s * (1 - s)

    return d_z


def tanh(z):

    """

    Implements the hyperbolic tangent activation

    Arguments:
    z -- the linear result of the forward propagation

    Returns:
    a -- the activation result of the forward propagation
    cache -- z, the linear forward stored for computing the backward propagation efficiently

    """

    # Previous formula, was causing an overflow error:
    # a = np.divide((np.exp(z)-np.exp(-z)),(np.exp(z)+np.exp(z)))
    # Current formula
    a = np.tanh(z)

    cache = z

    return a, cache


def tanh_backward(d_a, cache):

    """

    Implement the backward propagation for a single tanh unit

    Arguments:
    d_a -- post-activation gradient
    cache -- z, the linear forward stored for computing the backward propagation efficiently

    Returns:
    d_z -- gradient of the cost with respect to z

    """

    z = cache
    t, _ = tanh(z)
    d_z = d_a * (1-np.power(t, 2))

    return d_z


def relu(z):

    """

    Implementation of the RELU function

    Arguments:
    z -- the linear result of the forward propagation

    Returns:
    a -- the activation result of the forward propagation
    cache -- z, stored for computing the backward propagation efficiently

    """

    a = np.maximum(0, z)
    cache = z

    return a, cache


def relu_backward(d_a, cache):

    """

    Implement the backward propagation for a single RELU unit

    Arguments:
    d_a -- post-activation gradient
    cache -- z, the result of the forward propagation

    Returns:
    d_z -- gradient of the cost with respect to z

    """

    z = cache
    # converting dz to a correct object
    d_z = np.array(d_a, copy=True)

    # When z <= 0, you should set d_z to 0 as well.
    d_z[z <= 0] = 0
    d_z[z > 0] = 1
    d_z = d_a * d_z

    return d_z


def grands_to_vector(grads, layer_dims, batch_norm):

    """

    Roll all parameters dictionary into a single vector satisfying our specific required shape, used for
    gradient checking

    Arguments:
    grads -- dictionary of gradients of the cost with respect to parameters
    layer_dims -- list of each layer's dimension
    batch_norm -- boolean, if batch normalization will be used or not

    Returns:
    theta -- rolled out vector of gradients

    """

    # used to add the first entry of the new vector
    count = 0

    # number of layers in the network
    layers_len = len(layer_dims)

    for l in range(1, layers_len):
        vector = grads["d_w"]["d_w_" + str(l)]
        vector = np.reshape(vector, (-1, 1))

        if count == 0:
            theta = vector
        else:
            theta = np.concatenate((theta, vector), axis=0)

        count += 1

    for l in range(1, layers_len):
        vector = grads["d_b"]["d_b_" + str(l)]
        vector = np.reshape(vector, (-1, 1))
        theta = np.concatenate((theta, vector), axis=0)

    if batch_norm:
        for l in range(1, layers_len-1):
            vector = grads["d_gamma"]["d_gamma_" + str(l)]
            vector = np.reshape(vector, (-1, 1))
            theta = np.concatenate((theta, vector), axis=0)

        for l in range(1, layers_len-1):
            vector = grads["d_beta"]["d_beta_" + str(l)]
            vector = np.reshape(vector, (-1, 1))
            theta = np.concatenate((theta, vector), axis=0)

    return theta


def parameters_to_vector(parameters, layer_dims, batch_norm):

    """

    Roll all parameters dictionary into a single vector satisfying our specific required shape, used for
    gradient checking

    Arguments:
    parameters -- dictionary of parameters
    layer_dims -- list of each layer's dimension
    batch_norm -- boolean, if batch normalization will be used or not

    Returns:
    theta -- rolled out vector of parameters

    """

    # used to add the first entry of the new vector
    count = 0

    # number of layers in the network
    layers_len = len(layer_dims)

    for l in range(1, layers_len):
        vector = parameters["w"]["w_"+str(l)]
        vector = np.reshape(vector, (-1, 1))

        if count == 0:
            theta=vector
        else:
            theta=np.concatenate((theta, vector), axis=0)

        count += 1

    for l in range(1, layers_len):
        vector = parameters["b"]["b_"+str(l)]
        vector = np.reshape(vector, (-1, 1))
        theta=np.concatenate((theta, vector), axis=0)

    if batch_norm:
        for l in range(1, layers_len-1):
            vector = parameters["gamma"]["gamma_" + str(l)]
            vector = np.reshape(vector, (-1, 1))
            theta = np.concatenate((theta, vector), axis=0)

        for l in range(1, layers_len-1):
            vector = parameters["beta"]["beta_" + str(l)]
            vector = np.reshape(vector, (-1, 1))
            theta = np.concatenate((theta, vector), axis=0)

    return theta


def parameters_to_dictionary(theta, layer_dims, batch_norm):

    """

    Unroll all parameters dictionary from a single vector satisfying our specific required shape,used for
    gradient checking

    Arguments:
    parameters -- vector of parameters
    layer_dims -- list of each layer's dimension
    batch_norm -- boolean, if batch normalization will be used or not

    Returns:
    parameters -- dictionary of parameters

    """

    # number of layers in the network
    layers_len = len(layer_dims)

    # sums the sizes of parameters as we extract them from the vector
    position = 0
    parameters = {}
    parameter={}

    for l in range(1, layers_len):
        length = layer_dims[l] * layer_dims[l - 1]
        parameter["w_" + str(l)] = theta[position:position + length].reshape((layer_dims[l], layer_dims[l - 1]))
        position += length

    parameters["w"] = parameter
    parameter = {}

    for l in range(1, layers_len):
        length = layer_dims[l]
        parameter["b_" + str(l)] = theta[position:position + length].reshape((layer_dims[l], 1))
        position += length
    parameters["b"] = parameter

    if batch_norm:
        parameter = {}

        for l in range(1, layers_len-1):
            length = layer_dims[l]
            parameter["gamma_" + str(l)] = theta[position:position + length].reshape((layer_dims[l], 1))
            position += length
        parameters["gamma"] = parameter

        parameter = {}

        for l in range(1, layers_len-1):
            length = layer_dims[l]
            parameter["beta_" + str(l)] = theta[position:position + length].reshape((layer_dims[l], 1))
            position += length
        parameters["beta"] = parameter

    return parameters


def one_hot_matrix(y, c):

    y = np.eye(c)[y.reshape(-1)].T

    return y


def load_dataset(train_dataset_path, test_dataset_path):

    train_dataset = h5py.File(train_dataset_path, "r")
    train_set_x_orig = np.array(train_dataset["train_set_x"][:])  # set features
    train_set_y_orig = np.array(train_dataset["train_set_y"][:])  # set labels

    test_dataset = h5py.File(test_dataset_path, "r")
    test_set_x_orig = np.array(test_dataset["test_set_x"][:])  # test set features
    test_set_y_orig = np.array(test_dataset["test_set_y"][:])  # test set labels

    classes = np.array(test_dataset["list_classes"][:])  # the list of classes

    train_set_y_orig = train_set_y_orig.reshape((1, train_set_y_orig.shape[0]))
    test_set_y_orig = test_set_y_orig.reshape((1, test_set_y_orig.shape[0]))

    return train_set_x_orig, train_set_y_orig, test_set_x_orig, test_set_y_orig, classes
