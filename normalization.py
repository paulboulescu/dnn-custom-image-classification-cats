
import numpy as np


def normalize_inputs(x):

    """

    Normalizes the input data of the network

    Arguments:
    x -- input data

    Returns:
    norm_x -- normalized data
    mu -- meand value
    sigma -- variance value

    """

    m = x.shape[1]
    mu = (1 / m) * np.sum(x, 1).reshape((x.shape[0],1))
    sigma = (1 / m) * np.sum(np.power(x, 2), 1).reshape((x.shape[0], 1))
    norm_x = (x - mu) / sigma

    return norm_x, mu, sigma


def batch_normaliation(z, gamma, beta, eps):

    """

    Normalizes the linear forward values of a single layer

    http://kratzert.github.io/2016/02/12/understanding-the-gradient-flow-through-the-batch-normalization-layer.html

    Arguments:
    z -- linear forward values to be normalized
    gamma -- hyper-parameter used to change the variance value - shape [n,1]
    beta -- hyper-parameter used to change the mean value - shape [n,1]
    eps -- integer, used broadcasting, prevents the denominator to be equal to 0

    Returns:
    z_hat -- normalized value
    batch_norm_cache -- tuple of values cached in order to be used in backward propagation
    """

    n, m = z.shape

    # Step1: calculate mean
    mu = (1. / m) * np.sum(z, axis=1, keepdims=True)  # (n,1)

    # Step2: subtract mean vector of every trainings example
    xmu = z - mu  # (n,m)

    # Step3: following the lower branch - calculation denominator
    sq = xmu ** 2  # (n,m)

    # Step4: calculate variance
    var = (1. / m) * np.sum(sq, axis=1, keepdims=True)  # (n,1)

    # Step5: add eps for numerical stability, then sqrt
    sqrtvar = np.sqrt(var + eps)  # (n,1)

    # Step6: invert sqrtwar
    ivar = 1. / sqrtvar  # (n,1)

    # Step7: execute normalization
    xhat = xmu * ivar  # (n,m)

    # Step8: Nor the two transformation steps
    gammax = gamma * xhat  # (n,m)

    # Step9
    z_hat = gammax + beta  # (n,m)

    # Store intermediate
    batch_norm_cache = (xhat, gamma, xmu, ivar, sqrtvar, var, eps)

    return z_hat, batch_norm_cache
