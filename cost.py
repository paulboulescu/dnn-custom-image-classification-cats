
from utils import *


def compute_cost(last_a, y, softmax=False):

    """
    Implements the cost function

    Arguments:
    last_a -- the activation of the last layer in the network
    y -- labels data set

    Returns:
    cost -- cost of the network
    """

    m = y.shape[1]

    if softmax:
        # computes loss from last_a and y with softmax
        cost = -(1 / m) * np.sum(np.multiply(y, np.log(last_a)))
    else:
        # computes loss from last_a and y without softmax
        cost = -(1 / m) * np.sum(np.multiply(y, np.log(last_a)) + np.multiply((1 - y), np.log(1 - last_a)))

    # to make sure your cost's shape is the one expected
    cost = np.squeeze(cost)

    return cost
