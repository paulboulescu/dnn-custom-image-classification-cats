
from utils import *


def linear_backward(d_z, cache, dropout=True):

    """
    Calculates the backward propagation for the Linear portion of a single layer

    Arguments:
    d_z -- gradient of the cost with respect to the linear output of the current layer
    cache -- tuple of values (a_prev, w, b) determined in the forward propagation of the current layer
    dropout -- boolean, if dropout is applied or not

    Returns:
    d_a_prev -- gradient of the cost with respect to the activation (previous layer l-1), [same shape as a_prev]
    d_w -- gradient of the cost with respect to w (current layer l) [same shape as w]
    d_b -- gradient of the cost with respect to b (current layer l) [same shape as b]

    """

    if dropout:
        a_prev, w, b, _, _ = cache
    else:
        a_prev, w, b = cache

    m = a_prev.shape[1]
    d_w = (1. / m) * np.dot(d_z, a_prev.T)
    d_b = (1. / m) * np.sum(d_z, axis=1, keepdims=True)
    d_a_prev = np.dot(w.T, d_z)

    return d_a_prev, d_w, d_b


def batch_norm_backward(d_z_hat, cache):

    """

    Backward probagation for Batch Normalization over a single layer

    http://kratzert.github.io/2016/02/12/understanding-the-gradient-flow-through-the-batch-normalization-layer.html

    Arguments:
    d_z_hat -- gradient of the cost with respect to the normalized linear forward value
    cache -- tuple of values cached in the forward propagation

    Returns:
    d_z -- gradient of the cost with respect to the linear forward values
    d_gamma -- gradient of the cost with respect to the gamma hyper-parameter
    d_beta -- gradient of the cost with respect to the beta hyper-parameter

    """

    # Unfold the variables stored in cache
    xhat, gamma, xmu, ivar, sqrtvar, var, eps = cache

    # Get the dimensions of the input/output
    n, m = d_z_hat.shape  # (n,m)

    # Step9
    d_beta = 1./m * np.sum(d_z_hat, axis=1, keepdims=True)  # (n,1)
    dgammax = d_z_hat  # not necessary, but more understandable  # (n,m)

    # Step8
    d_gamma = 1./m * np.sum(dgammax * xhat, axis=1, keepdims=True)  # (n,1)
    dxhat = dgammax * gamma  # (n,m)

    # Step7
    divar = np.sum(dxhat * xmu, axis=1, keepdims=True)  # (n,1)
    dxmu1 = dxhat * ivar  # (n,m)

    # Step6
    dsqrtvar = (-1. / (sqrtvar ** 2)) * divar  # (n,1)

    # Step5
    dvar = (1 / 2) * (1. / np.sqrt(var + eps)) * dsqrtvar  # (n,1)

    # Step4
    dsq = (1. / m) * (np.ones((n, m)) * dvar)  # (n,m)

    # Step3
    dxmu2 = 2 * xmu * dsq  # (n,m)

    # Step2
    dx1 = (dxmu1 + dxmu2)  # (n,m)
    dmu = -1 * np.sum(dxmu1 + dxmu2, axis=1, keepdims=True)  # (n,1)

    # Step1
    dx2 = (1. / m) * (np.ones((n, m)) * dmu)  # (n,m)

    # Step0
    d_z = dx1 + dx2  # (n,m)

    return d_z, d_gamma, d_beta


def activation_backward(d_a, activation_cache, activation):

    """

    Calculates the backward propagation for the activation portion of a single layer
    
    Arguments:
    d_a -- post-activation gradient for current layer
    activation_cache -- tuple of values (linear_cache, activation_cache) stored for computing backward propagation
    activation -- the activation function used in this layer, stored as a text string: "sigmoid", "tanh", or "relu"
    
    Returns:
    d_z = gradient of the cost with respect to the linear forward value

    """

    if activation == "relu":
        d_z = relu_backward(d_a, activation_cache)
    elif activation == "sigmoid":
        d_z = sigmoid_backward(d_a, activation_cache)
    elif activation == "tanh":
        d_z = tanh_backward(d_a, activation_cache)

    return d_z


def backward_propagation(last_a, y, caches, activation="relu", batch_norm=True, dropout=True, softmax=False):

    """

    Calculates the backward propagation over the entire network

    Arguments:
    last_a -- last activation of the network
    y -- labels data set
    caches -- tuple of values from the forward propagation
    activation -- activation function used across the hidden layers
    batch_norm -- boolean, if batch normalization is used or not
    dropout -- boolean, if dropout is used or not
    softmax -- boolean, if softmax is used or not

    Returns:
    grads -- dictionary of the cost with respect to the parameters
    """

    grads = {}
    d_w = {}
    d_b = {}

    if batch_norm:
        d_gamma = {}
        d_beta = {}

    # The number of layers
    layers_len = len(caches) + 1
    y = y.reshape(last_a.shape)
    current_cache = caches[-1]
    linear_cache, batch_norm_cache, activation_cache = current_cache

    if softmax:
        d_z = last_a-y
    else:
        d_last_a = - (np.divide(y, last_a) - np.divide(1 - y, 1 - last_a))  # derivative of cost with respect to last_a
        d_z = activation_backward(d_last_a, activation_cache, 'sigmoid')

    d_a_prev, d_w["d_w_" + str(layers_len-1)], d_b["d_b_" + str(layers_len-1)] = \
        linear_backward(d_z, linear_cache, False)

    for l in reversed(range(1, layers_len-1)):

        current_cache = caches[l-1]

        linear_cache, batch_norm_cache, activation_cache = current_cache

        if dropout:
            _, _, _, d, layer_keep_probs = linear_cache

            # Step 1: Apply drop down mask to shut down the same neurons as during the forward propagation
            d_a_prev = np.multiply(d_a_prev, d)

            # Step 2: Scale the value of neurons that haven't been shut down
            d_a_prev = d_a_prev / layer_keep_probs

        d_z = activation_backward(d_a_prev, activation_cache, activation)

        if batch_norm:
            d_z, d_gamma["d_gamma_" + str(l)], d_beta["d_beta_" + str(l)] = batch_norm_backward(d_z, batch_norm_cache)

        d_a_prev, d_w["d_w_" + str(l)], d_b["d_b_" + str(l)] = linear_backward(d_z, linear_cache, dropout)
    
    if batch_norm:
        grads["d_gamma"] = d_gamma
        grads["d_beta"] = d_beta

    grads["d_w"] = d_w
    grads["d_b"] = d_b

    return grads
