
from forward import *
from cost import *

def gradient_check(parameters, grads, x, y, layers_dims, epsilon=1e-7, batch_norm=True, activation="relu",
                   batch_norm_eps=1e-8, softmax=False):

    """
    Checks if backward propagation computes correctly the gradients of the cost with respect to the parameters

    Arguments:
    parameters -- dictionary of parameters
    grads -- dictionary of parameters gradients, determined through backward propagation
    x -- input data set
    y -- labels data set
    layers_dims -- list of each layer's dimension
    epsilon -- difference used to approximate the gradient
    batch_norm -- boolean, if batch normalization is used or not
    activation -- activation function used over the hidden layers
    batch_norm_eps -- epsilon, prevents the denominator to be equal to 0
    softmax -- boolean, if softmax activation is used in the last layer or not

    Returns:
    difference -- difference between the backward propagation gradients and the gradient approximations
    """

    parameters_vector = parameters_to_vector(parameters, layers_dims, batch_norm)
    grad_vector = grands_to_vector(grads, layers_dims, batch_norm)
    num_parameters = parameters_vector.shape[0]
    j_plus = np.zeros((num_parameters, 1))
    j_minus = np.zeros((num_parameters, 1))
    gradapprox = np.zeros((num_parameters, 1))

    for i in range(num_parameters):

        # Compute J_plus[i]. Inputs: "parameters_values, epsilon". Output = "J_plus[i]".
        thetaplus_vector = np.copy(parameters_vector)
        thetaplus_vector[i][0] += epsilon
        last_a, _ = forward_propagation(x, parameters_to_dictionary(thetaplus_vector, layers_dims, batch_norm),
                                        layers_dims, batch_norm, activation, False, batch_norm_eps, softmax)
        j_plus[i] = compute_cost(last_a, y, softmax)

        # Compute J_minus[i]. Inputs: "parameters_values, epsilon". Output = "J_minus[i]".
        thetaminus_vector = np.copy(parameters_vector)
        thetaminus_vector[i][0] -= epsilon
        last_a, _ = forward_propagation(x, parameters_to_dictionary(thetaminus_vector, layers_dims, batch_norm),
                                        layers_dims, batch_norm, activation, False, batch_norm_eps, softmax)

        j_minus[i] = compute_cost(last_a, y, softmax)

        # Compute gradapprox[i]
        gradapprox[i] = (j_plus[i] - j_minus[i]) / (2 * epsilon)

    # Compare gradapprox to backward propagation gradients by computing difference:
    # calculates the Euclidean Distance
    numerator = np.linalg.norm(gradapprox - grad_vector)
    denominator = np.linalg.norm(gradapprox) + np.linalg.norm(grad_vector)
    difference = numerator / denominator

    if difference > 2e-7:
        print("There is a mistake in the backward propagation! difference = " + str(difference))
    else:
        print("Your backward propagation works perfectly fine! difference = " + str(difference))

    return difference
