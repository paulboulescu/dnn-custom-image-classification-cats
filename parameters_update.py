
import numpy as np


def update_parameters_with_gd(parameters, layers_dims, grads, learning_rate, batch_norm=True):

    """
    Updates the parameters using Gradient Descent

    Arguments:
    parameters -- contains current values of parameters
    layers_dims -- list of each layer's dimension
    grads -- contains current values of gradients, output of backward propagation
    learning_rate -- learning rate used to update parameters
    batch_norm -- boolean, if batch normalization is used or not

    Returns:
    parameters -- the updated parameters

    """

    # Number of layers in the neural network
    layers_len = len(layers_dims)

    for l in range(1, layers_len):
        parameters["w"]["w_" + str(l)] -= learning_rate * grads["d_w"]["d_w_" + str(l)]
        parameters["b"]["b_" + str(l)] -= learning_rate * grads["d_b"]["d_b_" + str(l)]

        # (!) Not sure if requied on the last layer but without it exp() would return +Infinit
        if batch_norm and l<layers_len-1:
            parameters["beta"]["beta_" + str(l)] -= learning_rate * grads["d_beta"]["d_beta_" + str(l)]
            parameters["gamma"]["gamma_" + str(l)] -= learning_rate * grads["d_gamma"]["d_gamma_" + str(l)]

    return parameters


def update_parameters_with_momentum(parameters,layers_dims, grads, v, beta, learning_rate, batch_norm=True):

    """
    Update parameters using Momentum

    Arguments:
    parameters -- contains current values of parameters
    layers_dims -- list of each layer's dimension
    grads -- contains current values of gradients, output of backward propagation
    v -- Momentum hyper-parameter
    beta -- Momentum hyper-parameter
    learning_rate -- learning rate used to update parameters
    batch_norm -- boolean, if batch normalization is used or not

    Returns:
    parameters -- the updated parameters
    v -- Momentum hyper-parameter

    """

    layers_len = len(layers_dims)  # number of layers in the neural networks

    # Momentum update for each parameter
    for l in range(1, layers_len):
        # compute velocities
        v["d_w_" + str(l)] = beta * v["d_w_" + str(l)] + (1 - beta) * grads["d_w"]["d_w_" + str(l)]
        v["d_b_" + str(l)] = beta * v["d_b_" + str(l)] + (1 - beta) * grads["d_b"]["d_b_" + str(l)]
        # update parameters
        parameters["w"]["w_" + str(l)] -= learning_rate * v["d_w_" + str(l)]
        parameters["b"]["b_" + str(l)] -= learning_rate * v["d_b_" + str(l)]

        # (!) Not sure if requied on the last layer but without it exp() would return + Infinit
        if batch_norm and l<layers_len-1:
            parameters["beta"]["beta_" + str(l)] -= learning_rate * grads["d_beta"]["d_beta_" + str(l)]
            parameters["gamma"]["gamma_" + str(l)] -= learning_rate * grads["d_gamma"]["d_gamma_" + str(l)]

    return parameters, v


def update_parameters_with_adam(parameters, layers_dims, grads, v, s, t, learning_rate=0.01, beta1=0.9, beta2=0.999, epsilon=1e-8,
                                batch_norm=True):

    """

    Update parameters with Adam

    Arguments:
    parameters -- contains current values of parameters
    layers_dims -- list of each layer's dimension
    grads -- contains current values of gradients, output of backward propagation
    v -- Adam hyper-parameter
    s -- Adam hyper-parameter
    t -- Adam counter used for bias correction
    learning_rate -- learning rate used to update parameters
    beta1 -- Adam weight
    beta2 -- Adam weight
    batch_norm -- boolean, if batch normalization is used or not

    Returns:
    parameters -- the updated parameters
    v -- Adam hyper-parameter
    s -- Adam hyper-parameter

    """

    layers_len = len(layers_dims)  # number of layers in the neural networks
    v_corrected = {}
    s_corrected = {}

    # Perform Adam update on all parameters
    for l in range(1, layers_len):

        # Moving average of the gradients
        v["d_w_" + str(l)] = beta1 * v["d_w_" + str(l)] + (1 - beta1) * grads["d_w"]["d_w_" + str(l)]
        v["d_b_" + str(l)] = beta1 * v["d_b_" + str(l)] + (1 - beta1) * grads["d_b"]["d_b_" + str(l)]

        # Compute bias-corrected first moment estimate.
        v_corrected["d_w_" + str(l)] = v["d_w_" + str(l)] / (1 - beta1 ** t)
        v_corrected["d_b_" + str(l)] = v["d_b_" + str(l)] / (1 - beta1 ** t)

        # Moving average of the squared gradients
        s["d_w_" + str(l)] = beta2 * s["d_w_" + str(l)] + (1 - beta2) * grads["d_w"]["d_w_" + str(l)] ** 2
        s["d_b_" + str(l)] = beta2 * s["d_b_" + str(l)] + (1 - beta2) * grads["d_b"]["d_b_" + str(l)] ** 2

        # Compute bias-corrected second raw moment estimate
        s_corrected["d_w_" + str(l)] = s["d_w_" + str(l)] / (1 - beta2 ** t)
        s_corrected["d_b_" + str(l)] = s["d_b_" + str(l)] / (1 - beta2 ** t)

        # Update parameters
        parameters["w"]["w_" + str(l)] = parameters["w"]["w_" + str(l)] - learning_rate * (
                v_corrected["d_w_" + str(l)] / (np.sqrt(s_corrected["d_w_" + str(l)]) + epsilon))
        parameters["b"]["b_" + str(l)] = parameters["b"]["b_" + str(l)] - learning_rate * (
                v_corrected["d_b_" + str(l)] / (np.sqrt(s_corrected["d_b_" + str(l)]) + epsilon))

        # (!) Not sure if requied on the last layer but without it exp() would return + Infinit
        if batch_norm and l<layers_len-1:
            parameters["beta"]["beta_" + str(l)] -= learning_rate * grads["d_beta"]["d_beta_" + str(l)]
            parameters["gamma"]["gamma_" + str(l)] -= learning_rate * grads["d_gamma"]["d_gamma_" + str(l)]

    return parameters, v, s
