# DNN Classifier with Custom Classes and Functions

## About
**Custom** Deep Neural Network project, used for classification. Trains, tests, and runs models. Built around a tuning module, capable of generating random hyper-parameters and using those hyper-parameters for training. It is capable of saving and loading models. It is capable of visualizing trained models costs.

## Instalation
1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/dnn-custom-image-classification-cats.git
```

2. Train and test
```
$ python deep_learning.py --random <number or threads> --epochs <number of epochs>
```


3. Visualize the Cost(Epochs) graphic for trained models
```
$ python deep_learning.py --view
```

4. Continue training
```
$ python deep_learning.py --train <number of epochs>
```

## Requirements
* No requirements

## Details
* Train, test, and run model
* Optimizers: Gradient Descent, Momentum, Adam
* Initializations: He, Xavier
* Regularization: Dropout
* Activations: ReLU, Tanh, Sigmoid, Softmax
* Forward and backward propagation
* Batch normalization
* Gradient check
* Command line interaction
* Save and load models

## Disclaimer
Created under the Deep Learning Specialization (Andrew Ng, Younes Bensouda Mourri, Kian Katanforoosh) - Coursera. The code uses ideas, datasets, algorithms, and code fragments presented in the Course.