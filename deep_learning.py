
from tunner import Tunner
import json
import sys, getopt


def main(argv):

    """
    Receives and handles the shell commands

    Arguments:
    argv -- command line arguments to be parsed

    (!) Features to be added:
    - delete
    - duplicate
    - generate random hyper-parameters values within a certain interval
    """

    try:
        opts, args = getopt.getopt(argv,"r:e:t:v",["random=", "epochs=", "train=", "view"])
        global tunner
    except getopt.GetoptError:
        # exception run when unidentified argument is passed or argument is missing
        # exits Python, "2" is Unix specific, could be replaced by "some error message"
        sys.exit(2)

    options = {}

    for opt, arg in opts:
        options[opt] = arg

    arguments = {}

    for opt, arg in args:
        arguments[opt] = arg

    if "-r" in options.keys() or "--random" in options.keys():
        if "-r" in options.keys():
            no_of_threads = int(options["-r"])
        else:
            no_of_threads = int(options["--random"])
        tunner.create_threads(no_of_threads)

        if "-e" in options.keys():
            no_of_epochs = int(options["-e"])
        elif "--epochs" in options.keys():
            no_of_epochs = int(options["--epochs"])
        else:
            no_of_epochs = 1
        tunner.train(no_of_epochs)

    elif "-t" in options.keys() or "--train" in options.keys():
        tunner.load_threads()

        if "-t" in options.keys():
            no_of_epochs = int(options["-t"])
        else:
            no_of_epochs = int(options["--train"])
        tunner.train(no_of_epochs)

    elif "-v" in options.keys() or "--view" in options.keys():
        tunner.load_threads()
        tunner.view()

# Provides project information
resources = {}

# Load the local resources file
with open('resources/resources.json') as f:
    data = json.load(f)
    resources["softmax"] = bool(data["softmax"])
    resources["train_dataset_path"] = data["train_dataset_path"]
    resources["test_dataset_path"] = data["test_dataset_path"]

# Initiate a Tunner instance
tunner = Tunner(resources)

# Checks if this is the main file
if __name__ == "__main__":
    # Run the terminal command
    main(sys.argv[1:])

# Create models with random hyper-parameters
# tunner.create_threads(3)    # number of models
# tunner.train(300)   # number of epochs

# Display previously trained threads
# tunner.load_threads()
# tunner.view()

# Continue training of previously trainedcreated models
# tunner.load_threads()
# tunner.train(400)   # number of epochs
