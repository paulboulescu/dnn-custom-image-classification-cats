
from utils import *


def initialize_velocity(parameters, layers_dims):

    """

    Initializes hyper-parameters for Momentum optimization algorithm

    Arguments:
    parameters -- weights and bias units of the network
    layers_dims -- list of dimensions for each layer in the network

    Returns:
    v -- dictionary of hyper-parameters used in Momentum optimization

    """

    # number of layers in the neural networks
    layers_len = len(layers_dims)
    v = {}

    for l in range(1, layers_len):
        v["d_w_" + str(l)] = np.zeros((parameters["w"]["w_" + str(l)].shape[0],
                                       parameters["w"]["w_" + str(l)].shape[1]))
        v["d_b_" + str(l)] = np.zeros((parameters["b"]["b_" + str(l)].shape[0],
                                       parameters["b"]["b_" + str(l)].shape[1]))

    return v


def initialize_adam(parameters, layers_dims):

    """

    Initializes hyper-parameters for Adam optimization algorithm

    Arguments:
    parameters -- weights and bias units of the network
    layers_dims -- list of dimensions for each layer in the network

    Returns:
    v -- dictionary of hyper-parameters used in Adam optimization
    s -- dictionary of hyper-parameters used in Adam optimization
    """

    layers_len = len(layers_dims)  # number of layers in the neural networks
    v = {}
    s = {}

    for l in range(1, layers_len):
        v["d_w_" + str(l)] = np.zeros((parameters["w"]["w_" + str(l)].shape[0], parameters["w"]["w_" + str(l)].shape[1]))
        v["d_b_" + str(l)] = np.zeros((parameters["b"]["b_" + str(l)].shape[0], parameters["b"]["b_" + str(l)].shape[1]))
        s["d_w_" + str(l)] = np.zeros((parameters["w"]["w_" + str(l)].shape[0], parameters["w"]["w_" + str(l)].shape[1]))
        s["d_b_" + str(l)] = np.zeros((parameters["b"]["b_" + str(l)].shape[0], parameters["b"]["b_" + str(l)].shape[1]))

    return v, s
