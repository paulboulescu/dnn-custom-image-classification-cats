
import matplotlib.pyplot as plt

class Visualization():

    """
    Used for displaying 'cost(epochs)' graphics for different models
    """

    def __init__(self):
        self.data = {}
        self.fig = plt.figure()
        self.colors=["b","g","r","c","m","y","k"]
        self.x={}

    def draw(self, data):
        self.data = data
        counter = 0

        for key, value in data.items():
            plt.plot(self.data[key].resources["costs"], color=self.colors[counter], label=str(key),
                     linewidth=2)
            counter += 1

        plt.xlabel('Epochs')
        plt.ylabel('Cost')
        plt.title('Training Costs')
        plt.legend()
        plt.show()
