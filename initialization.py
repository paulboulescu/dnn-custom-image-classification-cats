
from utils import *


def initialize_parameters_deep(layers_dims, initialization="he", batch_norm=True):

    """

    Performs initialization of the weights and bias units of the network

    Arguments:
    layers_dims -- list of dimensions of each layer
    initialization -- initialization algorithm used
    batch_norm --  boolean, if batch normalization is used or not

    Returns:
    parameters -- dictionary of parameters (weights, bias, gamma, beta)

    """

    parameters = {}

    # Number of layers in the network
    layers_len = len(layers_dims)

    weights={}
    bias_units={}

    if batch_norm:
        gamma={}
        beta={}

    for l in range(1, layers_len):
        if initialization == "he":
            # HE, used for ReLU activation
            weights["w_"+str(l)] = np.random.randn(layers_dims[l], layers_dims[l-1]) * np.sqrt(2 / layers_dims[l-1])
            bias_units["b_"+str(l)]=np.zeros((layers_dims[l], 1))
        elif initialization == "xavier":
            # Xavier, used for tanh activation
            weights["w_" + str(l)] = np.random.randn(layers_dims[l], layers_dims[l-1]) * np.sqrt(1 / layers_dims[l-1])
            bias_units["b_" + str(l)] = np.zeros((layers_dims[l], 1))

        if batch_norm and l<layers_len-1:
            gamma["gamma_" + str(l)] = np.ones((layers_dims[l], 1))
            beta["beta_"+str(l)] = np.zeros((layers_dims[l], 1))

    parameters["w"] = weights
    parameters["b"] = bias_units

    if batch_norm:
        parameters["gamma"] = gamma
        parameters["beta"] = beta

    return parameters
