
import numpy as np

def generate_dropout_keep_probs(layers_dims):

    """

    Used for dropout, calculates a threshold for each layer, based on its size

    Arguments:
    layers_dims -- list of each layer's dimension

    Returns:
    keep_probs -- list of each layer's keep probability

    """

    layers_len = len(layers_dims)

    # Create an empty keep probabilities array
    keep_probs = np.empty(0)

    # Previous formula:
    # determines the minimum and maxium of layer sizes across the network, excluding layer 0
    # max_value = layers_dims[1:-1].max()
    # min_value = layers_dims[1:-1].min()

    # Calculate the keep probabilities for each layer
    for l in range(1, layers_len-1):

        # Any further improvement should be tested in Google (i.e. e^((1-x)/20)*(4/5)+(1/5) )
        keep_probs = np.append(keep_probs, [round(np.exp((1-layers_dims[l])/20)*(4/5)+(1/5), 2)], axis=0)

    # Reshape in order to prevent shape related errors
    keep_probs = np.reshape(keep_probs, (keep_probs.shape[0], -1))

    return keep_probs


def apply_dropout(a, layer_keep_probs):

    """
    Used for dropout, determines a 0 or 1 value for each activation, based on its layer's predetermined threshold

    Arguments:
    a -- activation over which to apply dropout
    layer_keep_probs -- list of each layer's keep probability

    Returns:
    a -- activations to be dropped out
    d -- dropout mask
    layer_keeep_probs -- list of each layer's keep probability to be cached and used in backward probagation

    """

    # Create the dropout mask as an empty array with random values
    d = np.random.rand(a.shape[0], a.shape[1])

    # Convert entries of d to 0 or 1(using keep_prob as the threshold)
    d = d < layer_keep_probs

    # Shut down some neurons of A
    a = np.multiply(a, d)

    # Scale the value of neurons that haven't been shut down
    a = a / layer_keep_probs

    return a, d, layer_keep_probs
