
from regularization import *
from normalization import *
from utils import *


def linear_forward(a_prev, w, b):

    """

    Performs linear forward step in a single layer

    Arguments:
    a_prev -- previous activation
    w -- weights
    b -- bias units

    Returns:
    z - linear forward
    linear_cache -- tuple '(a_prev, w, b)'

    """

    z = np.dot(w, a_prev) + b
    linear_cache = (a_prev, w, b)

    return z, linear_cache


def activation_forward(z, activation="relu"):

    """

    Performs the activation step in a single layer

    Arguments:
    z -- linear forward
    activation -- function used for activation

    Returns:
    a -- activation result
    activation_cache -- value of 'z'

    """

    if activation == "sigmoid":
        a, activation_cache = sigmoid(z)
    elif activation == "relu":
        a, activation_cache = relu(z)
    elif activation == "tanh":
        a, activation_cache = tanh(z)
    elif activation== "softmax":
        t = np.exp(z)
        a = t / np.sum(t, axis=0).reshape(1, t.shape[1])
        activation_cache = z

    return a, activation_cache


def forward_propagation(x, parameters, layers_dims, batch_norm=True, activation="relu", dropout=True,
                        batch_norm_eps=1e-8, softmax=False):

    """

    Performs forward propagation over the entire network

    Arguments:
    x -- input data
    parameters -- weights and bias units used in forward propagation
    layers_dims -- list of each layer dimension
    batch_norm -- boolean, if batch normalization will be used or not
    activation -- activation used
    dropout -- boolean, if dropout will be used or not
    batch_norm_eps -- batch normalization epsilon, used for stability
    softmax -- boolean, if softmax is used or not

    Returns:
    last_a -- last activation of the network
    caches -- list of tuples used in backward propagation '(linear_cache, batch_norm_cache, activation_cache)'

    """

    # Number of layers in the network
    layers_len = len(layers_dims)

    # (!) L2 regularization to be added as an option

    # If dropout is used, keep probabilities will be generated
    if dropout:
        keep_probs = generate_dropout_keep_probs(layers_dims)

    # List which will hold cached values used in backward propagation
    caches = []
    a = x

    # loops over the hidden layers
    for l in range(1, layers_len-1):

        a_prev = a

        # Linear function
        z, linear_cache = linear_forward(a_prev, parameters["w"]["w_" + str(l)], parameters["b"]["b_" + str(l)])

        # If True, applies batch normalization
        if batch_norm:
            z, batch_norm_cache = batch_normaliation(z, parameters["gamma"]["gamma_" + str(l)],
                                                     parameters["beta"]["beta_" + str(l)], batch_norm_eps)
        else:
            batch_norm_cache = None

        # Activation function
        a, activation_cache = activation_forward(z, activation)

        # If True, applies dropout
        if dropout:
            a, d, layer_keep_probs = apply_dropout(a, keep_probs[l-1])
            linear_cache = linear_cache + (d,layer_keep_probs,)

        # Adds useful information to cache, to be used in backward propagation
        cache = (linear_cache, batch_norm_cache, activation_cache)
        caches.append(cache)

    last_z, linear_cache = linear_forward(a, parameters["w"]["w_" + str(layers_len-1)], parameters["b"]["b_" + str(layers_len-1)])

    # No batch normalization in the last layer
    batch_norm_cache = None

    if softmax:
        last_a, activation_cache = activation_forward(last_z, "softmax")
    else:
        last_a, activation_cache = activation_forward(last_z, "sigmoid")

    last_cache = (linear_cache, batch_norm_cache, activation_cache)
    caches.append(last_cache)

    return last_a, caches
