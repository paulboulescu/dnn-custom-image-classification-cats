
from initialization import *
from optimization import  *
from backward import *
from parameters_update import *
from debugging import *


class Model:

    # (!) Softmax and one-vs-all to be accommodated
    # (!) RMSprop to be added
    # (!) Learning rate decay to be added
    # (!) Threshold, F1 - precision and recall to be added

    # When tunning, the program will choose from these options
    optimizers = ["gd", "momentum", "adam"]
    initializations = ["he", "xavier"]
    activations = ["relu", "tanh"]

    def __init__(self, name, x, y, layers_dims, optimizer="adam", initialization="he", batch_norm=True,
                 learning_rate=0.0007, mini_batch_size=2048, beta1=0.9, beta2=0.999, epsilon=1e-8, activation="relu",
                 dropout=True, batch_norm_eps=1e-8, softmax=False, costs="None", parameters="None", v="None", s="None",
                 adam_counter="None"):

        """
        Initiates a L-layer neural network ((Linear -> Activation)*(L-1)) -> (Linear -> Sigmoid)

        Arguments:
        name -- name of the network
        x -- training dataset
        y -- labels dataset
        layers_dims -- list of each layer's dimension
        optimizer -- optimization algorithm to be used (gd, momentum, adam)
        initialization -- initialization algorithm to be used (he, xavier)
        batch_norm -- boolean, if batch normalization to be used or not
        learning_rate -- learning rate for gradient descent update
        mini_batch_size -- a power of 2 value, i.e. 2, 4, 8, 16 etc.
        beta1 -- beta1 optimization hyper-parameters
        beta2 -- beta1 optimization hyper-parameters
        epsilon -- small random value that prevents denominator = 0 when Adam optimization is used
        activation -- activation function used for the hidden layers (relu, tanh)
        dropout -- Boolean, if dropout to be used or not
        batch_norm_eps -- integer, used broadcasting, prevents the denominator to be equal to 0
        softmax -- boolean, if softmax to be used or not
        costs -- used to store the costs of pre-trained models
        parameters - used to store the parameters of pre-trained models
        v -- dictionary of hyper-parameters used in Adam optimization
        s -- dictionart of hyper-parameters used in Adam optimization
        adam_counter -- used to store the bias correction "t" value of pre-trained models

        Returns:
        -
        """

        self.resources={}
        self.resources["name"]=name
        self.resources["layers_dims"] = layers_dims
        self.resources["optimizer"] = optimizer
        self.resources["initialization"] = initialization
        self.resources["batch_norm"] = batch_norm
        self.resources["learning_rate"] = learning_rate
        self.resources["mini_batch_size"] = mini_batch_size
        self.resources["beta1"] = beta1
        self.resources["beta2"] = beta2
        self.resources["epsilon"] = epsilon
        self.resources["activation"] = activation
        self.resources["dropout"] = dropout
        self.resources["batch_norm_eps"] = batch_norm_eps
        self.resources["softmax"] = softmax
        self.resources["v"] = "None"
        self.resources["s"] = "None"
        self.resources["adam_counter"] = "None"

        # Normalizing set
        self.x, self.resources["mu"], self.resources["sigma"] = normalize_inputs(x)
        self.y = y
        self.resources["costs"] = np.array([])
        self.resources["parameters"] = initialize_parameters_deep(layers_dims, initialization, batch_norm)

        # Initializing the optimizer
        if self.resources["optimizer"] == "gd":
            # No initialization required for gradient descent
            pass
        elif self.resources["optimizer"] == "momentum":
            if v == "None":
                self.resources["v"] = initialize_velocity(self.resources["parameters"], self.resources["layers_dims"])
            else:
                self.resources["v"] = v.copy()
        elif self.resources["optimizer"] == "adam":
            if s == "None":
                self.resources["v"], self.resources["s"] = initialize_adam(self.resources["parameters"],
                                                                           self.resources["layers_dims"])
            else:
                self.resources["v"] = v.copy()
                self.resources["s"] = s.copy()

            # Initializing the counter required for Adam update
            if adam_counter == "None":
                self.resources["adam_counter"] = 0
            else:
                self.resources["adam_counter"] = adam_counter

    def json_to_object(data):

        """
        When using a pre-trained model, this function converts the saved parameters and hyper-parameters from a
        JSON format to Python objects
        """

        toObject = {}
        toObject["layers_dims"] = np.array(data["layers_dims"])
        layers_len = len(toObject["layers_dims"])
        toObject["parameters"] = {}
        toObject["parameters"]["w"] = {}
        toObject["parameters"]["b"] = {}
        toObject["batch_norm"] = data["batch_norm"]

        if toObject["batch_norm"]:
            toObject["parameters"]["gamma"] = {}
            toObject["parameters"]["beta"] = {}

        for l in range(1, layers_len):
            toObject["parameters"]["w"]["w_" + str(l)] = np.array(data["parameters"]["w"]["w_" + str(l)])
            toObject["parameters"]["b"]["b_" + str(l)] = np.array(data["parameters"]["b"]["b_" + str(l)])

            if toObject["batch_norm"] and l < layers_len - 1:
                toObject["parameters"]["gamma"]["gamma_" + str(l)] = np.array(data["parameters"]["gamma"]["gamma_" + str(l)])
                toObject["parameters"]["beta"]["beta_" + str(l)] = np.array(data["parameters"]["beta"]["beta_" + str(l)])

        toObject["optimizer"] = data["optimizer"]
        toObject["v"] = "None"
        toObject["s"] = "None"

        if toObject["optimizer"] == "momentum":
            toObject["v"] = {}

            for l in range(1, layers_len):
                toObject["v"]["d_w_" + str(l)] = np.array(data["v"]["d_w_" + str(l)])
                toObject["v"]["d_b_" + str(l)] = np.array(data["v"]["d_b_" + str(l)])

        elif toObject["optimizer"] == "adam":
            toObject["v"] = {}
            toObject["s"] = {}

            for l in range(1, layers_len):
                toObject["v"]["d_w_" + str(l)] = np.array(data["v"]["d_w_" + str(l)])
                toObject["v"]["d_b_" + str(l)] = np.array(data["v"]["d_b_" + str(l)])
                toObject["s"]["d_w_" + str(l)] = np.array(data["s"]["d_w_" + str(l)])
                toObject["s"]["d_b_" + str(l)] = np.array(data["s"]["d_b_" + str(l)])

        toObject["name"] = data["name"]
        toObject["initialization"] = data["initialization"]
        toObject["learning_rate"] = data["learning_rate"]
        toObject["beta1"] = data["beta1"]
        toObject["beta2"] = data["beta2"]
        toObject["epsilon"] = data["epsilon"]
        toObject["activation"] = data["activation"]
        toObject["dropout"] = data["dropout"]
        toObject["batch_norm_eps"] = data["batch_norm_eps"]
        toObject["softmax"] = data["softmax"]
        toObject["adam_counter"] = data["adam_counter"]
        toObject["mini_batch_size"] = data["mini_batch_size"]
        toObject["costs"] = np.array(data["costs"])

        return toObject


    def object_to_json(self):

        """
        When models are trained, this function converts parameters and hyper-parameters to JSON format for saving
        """

        layers_len = len(self.resources["layers_dims"])
        parameters = {}
        parameters["w"]={}
        parameters["b"]={}

        if self.resources["batch_norm"]:
            parameters["gamma"] = {}
            parameters["beta"] = {}

        for l in range(1, layers_len):
            parameters["w"]["w_" + str(l)]=self.resources["parameters"]["w"]["w_" + str(l)].tolist()
            parameters["b"]["b_" + str(l)] = self.resources["parameters"]["b"]["b_" + str(l)].tolist()

            if self.resources["batch_norm"] and l < layers_len - 1:
                parameters["gamma"]["gamma_" + str(l)] = self.resources["parameters"]["gamma"]["gamma_" + str(l)].tolist()
                parameters["beta"]["beta_" + str(l)] = self.resources["parameters"]["beta"]["beta_" + str(l)].tolist()

        v = "None"
        s = "None"

        if self.resources["optimizer"] == "momentum":
            v = {}

            for l in range(1, layers_len):
                v["d_w_" + str(l)] = self.resources["v"]["d_w_" + str(l)].tolist()
                v["d_b_" + str(l)] = self.resources["v"]["d_b_" + str(l)].tolist()

            s = "None"
        elif self.resources["optimizer"] == "adam":
            v = {}
            s = {}

            for l in range(1, layers_len):
                v["d_w_" + str(l)] = self.resources["v"]["d_w_" + str(l)].tolist()
                v["d_b_" + str(l)] = self.resources["v"]["d_b_" + str(l)].tolist()
                s["d_w_" + str(l)] = self.resources["s"]["d_w_" + str(l)].tolist()
                s["d_b_" + str(l)] = self.resources["s"]["d_b_" + str(l)].tolist()

        model_parameters = {"name": self.resources["name"],
                            "parameters": parameters,
                            "v": v,
                            "s": s,
                            "adam_counter": self.resources["adam_counter"]
                            }

        model_hyperparameters = {"name": self.resources["name"],
                                 "layers_dims": self.resources["layers_dims"].tolist(),
                                 "optimizer": self.resources["optimizer"],
                                 "initialization": self.resources["initialization"],
                                 "batch_norm": self.resources["batch_norm"],
                                 "learning_rate": self.resources["learning_rate"],
                                 # de schimbat int cu tolist
                                 "mini_batch_size": int(self.resources["mini_batch_size"]),
                                 "beta1": self.resources["beta1"],
                                 "beta2": self.resources["beta2"],
                                 "epsilon": self.resources["epsilon"],
                                 "activation": self.resources["activation"],
                                 "dropout": self.resources["dropout"],
                                 "batch_norm_eps": self.resources["batch_norm_eps"],
                                 "softmax": self.resources["softmax"],
                                 "costs": self.resources["costs"].tolist()
                                 }

        return model_parameters, model_hyperparameters

    def train(self, num_epochs):

        """
        Train the network over the dataset
        """

        self.num_epochs = num_epochs

        if self.resources["softmax"]:
            # Retrieve number of classes
            c = self.resources["layers_dims"][-1]
            # Transform y to c classes
            self.y = one_hot_matrix(self.y, c)

        # Optimization loop
        for i in range(self.num_epochs):
            minibatches = random_mini_batches(self.x, self.y, self.resources["mini_batch_size"], "fc")

            for minibatch in minibatches:

                # Select a minibatch
                (minibatch_x, minibatch_y) = minibatch

                # Forward propagation:
                last_a, caches = forward_propagation(minibatch_x, self.resources["parameters"],
                                                     self.resources["layers_dims"], self.resources["batch_norm"],
                                                     self.resources["activation"], self.resources["dropout"],
                                                     self.resources["batch_norm_eps"], self.resources["softmax"])

                # Computes cost
                cost = compute_cost(last_a, minibatch_y, self.resources["softmax"])

                # Backward propagation
                grads = backward_propagation(last_a, minibatch_y, caches, self.resources["activation"], self.resources["batch_norm"],
                                             self.resources["dropout"], self.resources["softmax"])

                # Update parameters
                if self.resources["optimizer"] == "gd":
                    self.resources["parameters"] = update_parameters_with_gd(self.resources["parameters"], self.resources["layers_dims"], grads,
                                                                self.resources["learning_rate"],
                                                                self.resources["batch_norm"])
                elif self.resources["optimizer"] == "momentum":
                    self.resources["parameters"], v = update_parameters_with_momentum(self.resources["parameters"], self.resources["layers_dims"], grads,
                                                                         self.resources["v"],
                                                                         self.resources["beta1"], self.resources["learning_rate"],
                                                                         self.resources["batch_norm"])
                elif self.resources["optimizer"] == "adam":
                    # Adam counter
                    self.resources["adam_counter"] = self.resources["adam_counter"] + 1
                    self.resources["parameters"], v, s = update_parameters_with_adam(self.resources["parameters"], self.resources["layers_dims"], grads,
                                                                        self.resources["v"],
                                                                        self.resources["s"], self.resources["adam_counter"], self.resources["learning_rate"],
                                                                        self.resources["beta1"], self.resources["beta2"], self.resources["epsilon"],
                                                                        self.resources["batch_norm"])

            self.resources["costs"] = np.append(self.resources["costs"], cost)

            if i % 100 == 0:
                print(self.resources["name"]+": cost after epoch %i is %f" % (i, cost))


    def predict(self, x, y):

        """
        Predicts the result of the model run over a data set

        Arguments:
        x - data set of examples you would like to label
        y - the labels of the data set for comparison

        Returns:
        a -- accuracy of predictions for the given dataset x over y
        """

        m = x.shape[1]
        p = np.zeros((1, m), dtype=int)

        # Normalize set
        x = (x - self.resources["mu"]) / self.resources["sigma"]

        # Forward propagation
        probabilities, _ = forward_propagation(x, self.resources["parameters"], self.resources["layers_dims"], self.resources["batch_norm"],
                                               self.resources["activation"], False, self.resources["batch_norm_eps"], self.resources["softmax"])

        # Compute cost
        # cost = compute_cost(probabilities, y, self.resources["softmax"])

        # Convert probas to 0/1 predictions
        if self.resources["softmax"]:
            p = np.argmax(probabilities, 0)
        else:
            for i in range(0, probabilities.shape[1]):
                if probabilities[0, i] > 0.5:
                    p[0, i] = 1
                else:
                    p[0, i] = 0

        # Calculate accuracy
        accuracy = np.sum(p == y) / float(m)

        return accuracy
